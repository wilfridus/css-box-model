<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>The Village Web</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="container">

		<div class="header">
			<h1 class="judul">Welcome to My Page</h1>
			<ul>
				<li><a href="#">Home</a></li>
				<li><a href="#">Abouts</a></li>
				<li><a href="#">Products</a></li>
				<li><a href="#">Services</a></li>
				<li><a href="#">Contact</a></li>
				<li class="right"><a href="#">Akun</a></li>
			</ul>
		</div>


		<div class="content cf">
			<div class="main">
				<h1>Judul Artikel</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis nesciunt vero explicabo laudantium corporis deleniti labore fuga, delectus dolore, non, harum dolores commodi earum possimus sed modi. Dolores aut perferendis neque, sit doloribus possimus praesentium consectetur, cupiditate magnam, consequatur ipsa vero ea et, non temporibus. Impedit non voluptas, quasi quidem suscipit vel! Culpa minus suscipit aperiam dolor consequatur maiores ipsa optio. At asperiores similique minima quas animi error reiciendis architecto inventore beatae, enim nulla, omnis totam dolor consequatur laborum nihil! Repudiandae laudantium asperiores quod accusantium ipsa eveniet sint ad blanditiis repellendus ullam nobis enim beatae, minus aliquam eligendi excepturi cum tenetur sit distinctio sapiente similique soluta! Beatae incidunt culpa, excepturi explicabo. Veniam repellat facilis iusto, totam soluta deserunt voluptate quibusdam iste pariatur. Iusto ullam obcaecati voluptatem impedit. Eaque dignissimos hic, cumque facilis. Quasi officiis optio laudantium neque excepturi quis reprehenderit molestias itaque minus eveniet, explicabo veniam dicta aliquam necessitatibus tempora, officia similique qui facilis voluptatibus expedita! Commodi saepe sequi vel iusto atque consequatur incidunt autem placeat architecto nemo est aliquid dolore eum vitae, error cumque quibusdam tempore, eaque ex amet cupiditate nobis quis praesentium porro. Hic neque incidunt deleniti, aspernatur accusantium, soluta odit dignissimos eius, quidem repellat officia voluptatem. Praesentium perferendis numquam ipsam magni necessitatibus aperiam iusto earum iure obcaecati debitis. Impedit rerum in soluta veniam numquam iusto facilis, ipsum iste fugit doloribus architecto libero ad doloremque sit vel repudiandae beatae cum tempora nisi odio debitis, similique dicta atque. Soluta vitae provident excepturi repellendus cumque quidem recusandae nostrum nihil atque non est odit dignissimos unde quis, tempore, quibusdam quam. Quisquam, optio accusantium aspernatur distinctio nostrum nemo excepturi tempore aut, inventore ullam, quia error dolores, doloremque commodi incidunt nihil! Error optio officiis quis laudantium ex minus architecto dignissimos esse a molestias fugiat, eum, eveniet culpa est nam aperiam nisi beatae, itaque perspiciatis nulla, magnam nemo temporibus. Nobis libero quidem, quas veritatis distinctio numquam optio officia repudiandae quod molestias inventore, ipsa vero repellat, adipisci laborum consectetur nisi. Molestias incidunt labore, velit magnam iure quasi voluptates dolor sed animi, aperiam ipsum tempore eos quo error et. Dolor, tempora cumque labore ea beatae. Nihil illo itaque, unde ea voluptatibus voluptas quam, ratione natus ad molestiae! Enim excepturi itaque maxime id, eum, in tempora dignissimos praesentium earum voluptatibus recusandae facilis aut. Eaque soluta quae sequi asperiores labore saepe beatae, recusandae suscipit maiores fugiat ut veritatis commodi ipsa perspiciatis aperiam molestiae nobis. Sed commodi incidunt iste aliquid mollitia! Officia vero, deserunt quisquam deleniti explicabo nulla molestias atque illum fugit odit dolorum laborum odio quibusdam laudantium saepe ipsa illo sapiente labore molestiae harum, iste. Libero, minus, consectetur. Vel quaerat officiis, labore adipisci officia voluptates inventore. Laborum minima provident, nulla deserunt neque ad accusantium atque molestias animi sapiente nesciunt corporis totam libero, voluptates, facilis, odit alias quidem. Deleniti minus unde illo qui esse nobis doloremque quos in ab corporis vero, accusantium delectus, voluptatem tempore modi tempora similique! Odio alias quas, beatae reprehenderit praesentium eligendi, sunt a excepturi voluptatem eos, nihil modi laborum magnam illum placeat similique mollitia fuga.</p>
			</div>
			<div class="sidebar">
				<h1>About me</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, hic, quae. Impedit error quasi fugiat, odit illo quis natus perferendis illum, expedita unde ullam culpa totam veniam saepe itaque! Esse, dicta, tempora laborum alias explicabo atque sequi architecto ipsam soluta numquam nulla dolores! Reiciendis amet nostrum officia, atque nam numquam ipsa pariatur fugiat distinctio minus delectus illo, impedit omnis, sapiente. Quos veritatis, laudantium laborum corporis vel obcaecati accusamus possimus explicabo neque voluptatem veniam suscipit incidunt odio nostrum eaque doloribus assumenda facere reprehenderit illum doloremque, repudiandae. Placeat assumenda vel perspiciatis accusamus voluptatum eum deleniti quae eligendi, iste consequuntur eos dicta ea.</p>
			</div>
		</div>

		<div class="footer">
			<p>Copyright All Right Reserved 2019</p>
		</div>
	</div>
</body>
</html>
